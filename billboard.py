# Libraries
from math import sqrt

import bmesh
import bpy


def generate_grid(self, context, cloud):
	if not "Clouds" in bpy.data.collections:
		coll = bpy.data.collections.new("Clouds")
		bpy.context.scene.collection.children.link(coll)
	else:
		coll = bpy.data.collections["Clouds"]

	settings = context.scene.clouds_settings
	# Deselect all
	bpy.ops.object.select_all(action='DESELECT')

	if cloud=="cumulus_billboard":
		res = settings.cumulus_billboard_res
	elif cloud=="altostratus_billboard":
		res = settings.altostratus_billboard_res

	if cloud=="cirrus" or cloud=="cirrocumulus":
		# Ray of sphere
		r = 1000
		# Number of squares
		squares = 60
	elif cloud=="altostratus" or cloud=="altostratus_mist":
		r = 1000
		squares = 60
		bpy.context.scene.cycles.volume_step_rate = 0.01
		bpy.context.scene.cycles.volume_preview_step_rate = 0.01
		bpy.context.scene.cycles.volume_max_steps = 30
	elif cloud=="cumulus" or cloud=="cumulus_mist":
		r = 1000
		squares = 20
		bpy.context.scene.cycles.volume_step_rate = 0.01
		bpy.context.scene.cycles.volume_preview_step_rate = 0.01
		bpy.context.scene.cycles.volume_max_steps = 30
	elif cloud=="cumulus_billboard" or cloud=="altostratus_billboard":
		bpy.context.scene.cycles.transparent_max_bounces = 300
		if res=="Low":
			n_layers = 8
			sub = 5
		elif res=="Mid":
			n_layers = 10
			sub = 10
		elif res=="High":
			n_layers = 15
			sub = 20
		# Layers
		sub_layer = 1
		r_layer = 1000*sub_layer
		s_layer = 15*sub_layer
		divz = n_layers-1
		#  Grid
		r = 1000*sub
		squares = 15*sub

	nverts = (squares*2+1)**2
	verts = [[i * j for j in range(3)] for i in range(nverts)]
	# generate array of hemisphere
	z=0
	for y in range(-squares, squares+1):
		for x in range(-squares, squares+1):
		    verts[z][0] = x
		    verts[z][1] = y
		    c = sqrt(x**2+y**2)
		    verts[z][2] = sqrt(r**2-c**2) # Z
		    z+=1
	nfaces = (squares*2)**2
	faces = [[i * j for j in range(4)] for i in range(nfaces)]
	j=0
	for i in range(nfaces):
		if i == ((squares*2)*(j+1)):
		    j=j+1
		faces[i][0] = i+j
		faces[i][1] = i+1+j
		faces[i][2] = i+squares*2+2+j
		faces[i][3] = i+squares*2+1+j

	if cloud=="cirrus":
		height = 6371+12
	elif cloud=="cirrocumulus":
		height = 6371+10
	elif cloud=="altostratus":
		height = 6371+4
	elif cloud=="altostratus_mist":
		height = 6371+4
	elif cloud=="altostratus_billboard":
		height = (6371+4)/sub
		h_plus = 4
	elif cloud=="cumulus":
		height = 6371+1
	elif cloud=="cumulus_mist":
		height = 6371+1
	elif cloud=="cumulus_billboard":
		height = (6371+1)/sub
		h_plus = 1

	mesh = bpy.data.meshes.new(cloud)
	mesh.from_pydata(verts, [], faces)
	mesh.update(calc_edges=True)
	obj = bpy.data.objects.new(cloud, mesh)
	coll.objects.link(obj)
	context.view_layer.objects.active = obj
	context.scene.objects[obj.name].select_set(True)
	# Change name
	context.object.name = cloud
	# Smooth shading
	if cloud=="cirrus" or cloud=="cirrocumulus":
		for f in obj.data.polygons:
			f.use_smooth = True
	objreturn = obj
	# Make it big as Earth
	bpy.ops.transform.resize(value=(height, height, height))
	obj.location[2] = -6371000
	context.scene.cursor.location = (0.0, 0.0, 0.0)
	bpy.ops.object.origin_set(type='ORIGIN_CURSOR')

	if cloud=="cumulus" or cloud=="altostratus" or cloud=="altostratus_mist" or cloud=="cumulus_mist":
		bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		obj = bpy.context.active_object.data
		bm = bmesh.from_edit_mesh(obj)
		plane = bm.verts[:]+bm.edges[:]+bm.faces[:]
		ret = bmesh.ops.extrude_face_region(bm, geom=plane)
		ext = ret["geom"]
		del ret
		v = [ele for ele in ext if isinstance(ele, bmesh.types.BMVert)]
		bmesh.ops.translate(bm, verts=v, vec=(0, 0, 1000))
		bmesh.update_edit_mesh(obj)
		bm.free()
		bpy.ops.object.mode_set(mode='OBJECT')
	elif cloud=="cumulus_billboard" or cloud=="altostratus_billboard":
		bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		obj = bpy.context.active_object.data
		bm = bmesh.from_edit_mesh(obj)
		xy = bm.edges[:]
		plane = bm.verts[:]+bm.edges[:]+bm.faces[:]
		ret = bmesh.ops.duplicate(bm, geom=xy)
		bmesh.ops.delete(bm, geom=plane, context='EDGES')
		l = ret["geom"]
		del ret
		edg = [ele for ele in l if isinstance(ele, bmesh.types.BMEdge)]
		ret = bmesh.ops.extrude_edge_only(bm, edges=edg)
		xyext = ret["geom"]
		del ret
		v = [ele for ele in l if isinstance(ele, bmesh.types.BMVert)]
		bmesh.ops.translate(bm, verts=v, vec=(0, 0, 1000))
		bmesh.update_edit_mesh(obj)
		bm.free()
		bpy.ops.object.mode_set(mode='OBJECT')
		bpy.ops.object.select_all(action='DESELECT')
		# Layers
		layer = "billboard"
		r = r_layer*sub_layer
		squares = s_layer*sub_layer
		nverts = (squares*2+1)**2
		verts = [[i * j for j in range(3)] for i in range(nverts)]
		i=0
		for y in range(-squares, squares+1):
			for x in range(-squares, squares+1):
				verts[i][0] = x
				verts[i][1] = y
				c = sqrt(x**2+y**2)
				verts[i][2] = sqrt(r**2-c**2) # Z
				i+=1
		nfaces = (squares*2)**2+squares*2-1
		faces = [[i * j for j in range(4)] for i in range(nfaces)]
		j=0
		for i in range(nfaces):
			if i!=squares*2+j*(squares*2+1):
				faces[i][0] = i
				faces[i][1] = i+1
				faces[i][2] = i+squares*2+2
				faces[i][3] = i+squares*2+1
			else:
				faces[i] = [0,0,0,0]
				j+=1
		height = (6371+h_plus+1/(divz+2))/sub_layer
		mesh = bpy.data.meshes.new(layer)
		mesh.from_pydata(verts, [], faces)
		mesh.update(calc_edges=True)
		obj = bpy.data.objects.new(layer, mesh)
		bpy.data.collections["Clouds"].objects.link(obj)
		context.view_layer.objects.active = obj
		context.scene.objects[obj.name].select_set(True)
		# Change name
		context.object.name = layer
		# Smooth shading
		for f in obj.data.polygons:
			f.use_smooth = True
		# Make it big as Earth
		bpy.ops.transform.resize(value=(height, height, height))
		obj.location[2] = -6371000
		context.scene.cursor.location = (0.0, 0.0, 0.0)
		bpy.ops.object.origin_set(type='ORIGIN_CURSOR')
		bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
		step_h = 1000/(divz+2)
		bpy.data.objects[layer].data.flip_normals()
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.select_all(action='DESELECT')
		obj = bpy.context.active_object.data
		bm = bmesh.from_edit_mesh(obj)
		t = bm.verts[:]+bm.edges[:]+bm.faces[:]
		for d in range(divz):
			ret = bmesh.ops.duplicate(bm, geom=t)
			t = ret["geom"]
			del ret
			v = [ele for ele in t if isinstance(ele, bmesh.types.BMVert)]
			bmesh.ops.translate(bm, verts=v, vec=(0, 0, step_h))
		bmesh.update_edit_mesh(obj)
		bm.free()
		bpy.ops.object.mode_set(mode='OBJECT')
		context.scene.objects[layer].select_set(True)
		context.scene.objects[cloud].select_set(True)
		bpy.ops.object.join()
		context.active_object.name = cloud
		objreturn = bpy.context.active_object

	return objreturn


def register():
	return None

def unregister():
	return None
